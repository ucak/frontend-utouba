import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './components/home/home.component';
import { NotFoundComponent } from './components/not-found/not-found.component';

const routes: Routes = [
  { path: '', component: HomeComponent },
  {
    path: 'contact',
    loadChildren: () => import('./modules/contact/contact.module')
      .then(mod => mod.ContactModule)
  },
  {
    path: 'faculte',
    loadChildren: () => import('./modules/faculte/faculte.module')
      .then(mod => mod.FaculteModule)
  },
  {
    path: 'universite/presentation',
    loadChildren: () => import('./modules/universite/presentation/presentation.module')
      .then(mod => mod.PresentationModule)
  },
  {
    path: 'universite/organigramme',
    loadChildren: () => import('./modules/universite/organigramme/organigramme.module')
      .then(mod => mod.OrganigrammeModule)
  },
  {
    path: 'universite/histoire',
    loadChildren: () => import('./modules/universite/histoire/histoire.module')
      .then(mod => mod.HistoireModule)
  },
  {
    path: 'ufr',
    loadChildren: () => import('./modules/ufr/ufr.module')
      .then(mod => mod.UfrModule)
  },
  {
    path: 'formation/offre-formation',
    loadChildren: () => import('./modules/formation/offre-formation/offre-formation.module')
      .then(mod => mod.OffreFormationModule)
  },
  {
    path: 'formation/admission',
    loadChildren: () => import('./modules/formation/admission/admission.module')
      .then(mod => mod.AdmissionModule)
  },
  {
    path: 'formation/licence',
    loadChildren: () => import('./modules/formation/licence/licence.module')
      .then(mod => mod.LicenceModule)
  },
  {
    path: 'formation/master',
    loadChildren: () => import('./modules/formation/master/master.module')
      .then(mod => mod.MasterModule)
  },
  {
    path: 'bibliotheque',
    loadChildren: () => import('./modules/bibliotheque/bibliotheque.module')
      .then(mod => mod.BibliothequeModule)
  },
  {
    path: 'blog',
    loadChildren: () => import('./modules/blog/blog.module')
      .then(mod => mod.BlogModule)
  },
  {
    path: 'news',
    loadChildren: () => import('./modules/news/news.module')
      .then(mod => mod.NewsModule)
  },
  {
    path: 'evenements',
    loadChildren: () => import('./modules/evenements/evenements.module')
      .then(mod => mod.EvenementsModule)
  },
  {
    path: 'emploi',
    loadChildren: () => import('./modules/emploi/emploi.module')
      .then(mod => mod.EmploiModule)
  },
  {
    path: 'date-important',
    loadChildren: () => import('./modules/dates/dates.module')
      .then(mod => mod.DatesModule)
  },
  {
    path: 'login',
    loadChildren: () => import('./modules/login/login.module')
      .then(mod => mod.LoginModule)
  },
  {
    path: 'search',
    loadChildren: () => import('./modules/search/search.module')
      .then(mod => mod.SearchModule)
  },
  {
    path: 'accueil', component: HomeComponent
  },

  { path: '**', component: NotFoundComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes,
    {
      scrollPositionRestoration: 'top',
      initialNavigation: 'enabledBlocking',
      onSameUrlNavigation: 'reload',
      anchorScrolling: 'enabled',
      relativeLinkResolution: 'legacy'
    }
  )],
  exports: [RouterModule],
  declarations: []
})
export class AppRoutingModule { }
