import {Component, OnInit, AfterViewInit, ViewChild, ElementRef, HostListener, Inject} from '@angular/core';
import { DOCUMENT } from '@angular/common';
import {TranslateService} from '@ngx-translate/core';
import { FormControl } from '@angular/forms';
import { Observable } from 'rxjs';
import {map, startWith} from 'rxjs/operators';
import { Router} from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit, AfterViewInit {

  @ViewChild('stickyMenu') menuElement: ElementRef;

  sticky = false;
  elementPosition: any;
  @ViewChild('searchbar') searchbar: ElementRef;
  searchText = '';
  toggleSearch = false;
  isVisible = false;
  showNav = false;
  myControl = new FormControl();
  //filteredOptions: Observable<string[]>;
  entry: string;

  constructor(@Inject(DOCUMENT) private document: Document,
              private translate: TranslateService,
              private router: Router) { }


  ngOnInit(): void {
    if(document.getElementById(sessionStorage.getItem('usedlanguage'))) {
      document.getElementById(sessionStorage.getItem('usedlanguage')).setAttribute('selected', 'selected');
      this.menuOrder();
    }
    /*this.filteredOptions = this.myControl.valueChanges
      .pipe(
        startWith(''),
        map(value => this._filter(value))
      );*/
  }

  ngAfterViewInit(): void {
    this.elementPosition = this.menuElement.nativeElement.offsetTop;
  }

  @HostListener('window:scroll')
  handleScroll(): void {
      if (this.getScrollOffset() > this.elementPosition){
        this.sticky = true;
      } else {
        this.sticky = false;
      }
  }

  getScrollOffset(): number{
    return  window.pageYOffset || document.documentElement.scrollTop || document.body.scrollTop || 0;
  }
  gotoENT(): void{
    this.document.location.href = 'http://13.36.120.170/';
  }

  useLanguage(language: string): void {
    sessionStorage.clear();
    sessionStorage.setItem('usedlanguage', language);
    this.translate.use(sessionStorage.getItem('usedlanguage'));
    this.menuOrder();
  }

  handleSubmit(event: any): Promise<boolean> {
    console.log(this.entry);
    return this.router.navigate(['search', {term: this.entry}]);
  }

  handleKeyUp(event: any): void {
     if (event.keyCode === 13) {
        this.handleSubmit(event);
     }
  }

  /*private _filter(value: string): string[] {
    if (value === '' || value === null || value.length < 2) {
      return [];																 
    }
    const filterValue = value.toLowerCase();
    return this.options.filter(option => option.toLowerCase().includes(filterValue));
  }*/

  openSearch(): void {
    this.isVisible = true;
    this.toggleSearch = true;
    this.searchbar.nativeElement.focus();
  }

  searchClose(): void {
    this.searchText = '';
    this.toggleSearch = false;
    this.isVisible = false;
  }

  menuOrder(): void {
    if (sessionStorage.getItem('usedlanguage') === 'ar') {
      this.document.getElementById('navbar').style.display = 'flex';
      this.document.getElementById('navbar').style.flexDirection = 'row-reverse';
      this.document.getElementById('row').style.marginLeft = 'auto';
    }
    else {
      this.document.getElementById('navbar').style.display = 'flex';
      this.document.getElementById('navbar').style.flexDirection = 'row';
    }
  }
}

