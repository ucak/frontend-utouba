import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-admission',
  templateUrl: './admission.component.html',
  styleUrls: ['./admission.component.scss']
})
export class AdmissionComponent implements OnInit {

  title = 'CRITÈRES D\'ADMISSION';
  subtitle = 'MODIFIÉ PAR Mourtalla AMAR LE 14/08/2021 - 00:58 | DIRCOM';
  sideBarTitle = 'CRITÈRES DE SELECTION ET DE PRÉSELECTION';
  imageUrl = 'assets/images/amphi_1000.jpg';
  paragraph = '';
  sideBarLien1 = 'assets/documents/admission/ADMISSION_UTOUBA.pdf';
  sideBarLien2 = 'assets/documents/admission/FILIERE_METIERS_DU_LIVRE.pdf';
  sideBarLien3 = 'assets/documents/admission/FILIERE_LANGUES_APPLIQUEES.pdf';
  sideBarLien4 = 'assets/documents/admission/INSTITUT_DES_LANGUES_ET_DES_METIERS_DU_LIVRE.pdf';
  sideBarLien5 = 'assets/documents/admission/INSTITUT_SUPERIEUR_DES_METIERS_DE_LA_SANTE.pdf';
  sideBarLien6 = 'assets/documents/admission/SCIENCES_AGRONOMIQUES_ET_DES_TECHNOLOGIES_ALIMENTAIRES_LICENCE_ELEVAGE_ET_PRODUCTIONS_ANIMALES.pdf';
  sideBarLien7 = 'assets/documents/admission/TRAITEMENT_AUTOMATIQUE_DES_LANGUES.pdf';
  sideBarLien8 = 'assets/documents/admission/UFR_DES_METIERS_ET_TECHNOLOGIES.pdf';
  sideBarLien9 = 'assets/documents/admission/UFR_ETUDES_ISLAMIQUES_ET_ARABES.pdf';

  sideBarLienUtiles1 = 'assets/documents/admission/ADMISSION_UTOUBA.pdf';
  sideBarLienUtiles2 = 'assets/documents/admission/FILIERE_METIERS_DU_LIVRE.pdf';
  sideBarLienUtiles3 = 'assets/documents/admission/FILIERE_LANGUES_APPLIQUEES.pdf';

  sideBarContact = 'Contacts';
  sideBarContact1 = 'Serigne Mor Faye';

  sideBarMotsCles = 'Mots Clès';
  sideBarMotsCles1 = 'Admission';
  sideBarMotsCles2 = 'Critères Admission';
  sideBarMotsCles3 = 'Amissibilité';



  constructor() { }

  ngOnInit(): void {
  }

}
