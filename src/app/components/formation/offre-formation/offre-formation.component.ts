import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-offre-formation',
  templateUrl: './offre-formation.component.html',
  styleUrls: ['./offre-formation.component.scss']
})
export class OffreFormationComponent implements OnInit {

  title = 'OFFRES DE FORMATION';
  subtitle = 'MODIFIÉ PAR IBRAHIMA SEYE LE 01/05/2021 - 15:58 | DIRCOM';
  sideBarTitle = 'DOCUMENTS A TELECHARGER';
  imageUrl = 'assets/images/salle_eng.jpg';

  constructor() { }

  ngOnInit(): void {
  }
}
