import { Component, OnInit } from '@angular/core';
import {sharedConstants} from '../../shared/constants/constants';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

  getLinkSocialNework(libelle: string): string {
    return sharedConstants.socialNetworkLinks[libelle];
  }
}
