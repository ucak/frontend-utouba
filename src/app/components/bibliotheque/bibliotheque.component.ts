import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-bibliotheque',
  templateUrl: './bibliotheque.component.html',
  styleUrls: ['./bibliotheque.component.scss']
})
export class BibliothequeComponent implements OnInit {

  sideBarTitle = 'DOCUMENTS A TELECHARGER';
  imageUrl = 'assets/images/mediatheque.jpeg';

  constructor() { }

  ngOnInit(): void {
  }

}
