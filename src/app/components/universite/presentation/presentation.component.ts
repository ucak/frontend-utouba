import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-presentation',
  templateUrl: './presentation.component.html',
  styleUrls: ['./presentation.component.scss']
})
export class PresentationComponent implements OnInit {

  sideBarTitle = 'DOCUMENTS A TELECHARGER';
  imageUrl = 'assets/home.png';
  constructor() { }

  ngOnInit(): void {
  }

}
