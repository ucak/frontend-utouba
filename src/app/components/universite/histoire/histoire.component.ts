import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-histoire',
  templateUrl: './histoire.component.html',
  styleUrls: ['./histoire.component.scss']
})
export class HistoireComponent implements OnInit {

  sideBarTitle = 'DOCUMENTS A TELECHARGER';
  imageUrl = 'assets/images/mosquee.jpg';


  constructor() { }

  ngOnInit(): void {
  }
}
