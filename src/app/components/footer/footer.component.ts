import { Component, OnInit } from '@angular/core';
import { sharedConstants } from '../../shared/constants/constants';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

  getLinkSocialNework(libelle: string): string {
    return sharedConstants.socialNetworkLinks[libelle];
  }

}
