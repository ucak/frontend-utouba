import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-emploi',
  templateUrl: './emploi.component.html',
  styleUrls: ['./emploi.component.scss']
})
export class EmploiComponent implements OnInit {

  title = 'EMPLOI';
  subtitle = 'MODIFIÉ PAR SERIGNE MBACKE DJILIW LE 30/05/2021 - 00:00 | DIRCOM';
  sideBarTitle = 'DOCUMENTS A TELECHARGER';
  imageUrl = 'assets/images/salle_eng.jpg';
  paragraph = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut\n' +
    '                     labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris\n' +
    '                     nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit\n' +
    '                     esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt\n' +
    '                     in culpa qui officia deserunt mollit anim id est laborum.';

  constructor() { }

  ngOnInit(): void {
  }

}
