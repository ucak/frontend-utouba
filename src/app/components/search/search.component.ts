import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
//import { searchBarMenu } from 'src/app/shared/models/searchBar.menu';
import admissionTest from 'src/assets/i18n/fr.json';



@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss']
})
export class SearchComponent implements OnInit {

  //searchables = searchBarMenu;
  admission = admissionTest; 
  links: any;
  term: string;

  constructor(private route: ActivatedRoute) {}

  ngOnInit(): void {
    this.route.params.subscribe( params => {
      console.log(params.term);
      if (params.term) {
        this.links = [];
        this.doSearch(params.term);
      }
    });
  }

  handleKeyUp(event: any): void {
    if (event.keyCode === 13) {
      this.handleSubmit();
    }
 }

  handleSubmit(): void {
    this.links = [];
    this.doSearch(this.term);
  }

  /*doSearch(value: string): void {
    for (const page of this.searchables) {
       if (page.keywords.includes(value)) {
         const result = {title: page.title, link: page.link, keywords: page.keywords};
         this.links.push(result);
       }
    }
  }*/
  
  doSearch(value: string): void {
    const page = this.admission;
    const TabClee=Object.keys(page)

    for (const clee of TabClee) {
      if (clee.includes('paragraphe')) {
        //console.log(clee[i]);
        if(page[clee].toLowerCase().includes(value.toLowerCase())) {
          //console.log(clee[i].split(".",3));
          const tableau = clee.split(".",3);
          if (tableau.length>=3) {
            //console.log("/"+tableau[0]+"/"+tableau[1]);
            const title = tableau[0]+"."+tableau[1]+".title";
            const connection = tableau[0]+"/"+tableau[1];
            const subtitle = tableau[0]+"."+tableau[1]+".subtitle";
            const result = {title: page[title], link: connection, keywords: page[subtitle]};
            this.links.push(result);
          }
          else {
            //console.log("/"+tableau[0]);
            const title = tableau[0]+".title";
            const connection = tableau[0];
            const subtitle = tableau[0]+".subtitle";
            const result = {title: page[title], link: connection, keywords: page[subtitle]};
            this.links.push(result);
          }
        }
      }
       
    //console.log(page[tb_clee[i]]);

    }

    /*for (const page of this.admission.content) {
      if (page.paragraph.toLowerCase().includes(value.toLowerCase())) {
        const result = {title: page.title, link: page.link, keywords: page.subtitle};
        this.links.push(result);
      }
   }*/
  }

}
