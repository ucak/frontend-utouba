import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {BlogRoutingModule} from './blog-routing.module';
import {BlogComponent} from '../../components/blog/blog.component';



@NgModule({
  imports: [
    CommonModule,
    BlogRoutingModule
  ],
  exports: [
    BlogComponent
  ],
  declarations: [
    BlogComponent
  ],
  providers: [
  ]
})
export class BlogModule { }
