import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {RouterModule, Routes} from '@angular/router';
import {BibliothequeComponent} from '../../components/bibliotheque/bibliotheque.component';
import {BlogComponent} from '../../components/blog/blog.component';



const routes: Routes = [
  { path: '', component: BlogComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BlogRoutingModule { }
