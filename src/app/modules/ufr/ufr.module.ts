import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatExpansionModule } from '@angular/material/expansion';
import { UfrRoutingModule } from './ufr-routing.module';
import { MatListModule } from '@angular/material/list';
import { UfrMetierTechnologiesComponent } from '../../components/ufr/ufr-metier-technologies/ufr-metier-technologies.component';
import { UfrIslamiqueArabeComponent } from '../../components/ufr/ufr-islamique-arabe/ufr-islamique-arabe.component';
import { UfrAgronomieAlimentaireComponent } from '../../components/ufr/ufr-agronomie-alimentaire/ufr-agronomie-alimentaire.component';
import { TranslateModule } from '@ngx-translate/core';
@NgModule({

  imports: [
    CommonModule,
    UfrRoutingModule,
    MatExpansionModule,
    MatListModule,
    TranslateModule
  ],
  declarations: [
    UfrMetierTechnologiesComponent,
    UfrIslamiqueArabeComponent,
    UfrAgronomieAlimentaireComponent
  ],
  exports: [
  ],
  providers: [
  ],
})
export class UfrModule { }
