import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { UfrAgronomieAlimentaireComponent } from 'src/app/components/ufr/ufr-agronomie-alimentaire/ufr-agronomie-alimentaire.component';
import { UfrIslamiqueArabeComponent } from 'src/app/components/ufr/ufr-islamique-arabe/ufr-islamique-arabe.component';
import { UfrMetierTechnologiesComponent } from 'src/app/components/ufr/ufr-metier-technologies/ufr-metier-technologies.component';

const routes: Routes = [
  { path: 'metiers-technologies', component : UfrMetierTechnologiesComponent},
  { path: 'islamique-arabe', component : UfrIslamiqueArabeComponent},
  { path: 'agronomie-alimentaire', component : UfrAgronomieAlimentaireComponent},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UfrRoutingModule { }
