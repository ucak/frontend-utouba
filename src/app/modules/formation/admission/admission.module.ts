import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AdmissionRoutingModule } from './admission-routing.module';
import { AdmissionComponent } from 'src/app/components/formation/admission/admission.component';
import {TemplateModule} from '../../template/template.module';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  imports: [
    CommonModule,
    TemplateModule,
    TranslateModule,
    AdmissionRoutingModule
  ],
  exports: [
    AdmissionComponent
  ],
  declarations: [
    AdmissionComponent
  ],
  providers: [
  ]
})
export class AdmissionModule { }
