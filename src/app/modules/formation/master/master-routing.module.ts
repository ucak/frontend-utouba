import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {RouterModule, Routes} from '@angular/router';
import {LicenceComponent} from '../../../components/formation/licence/licence.component';
import {MasterComponent} from '../../../components/formation/master/master.component';

const routes: Routes = [
  { path: '', component: MasterComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MasterRoutingModule { }
