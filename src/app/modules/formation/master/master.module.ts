import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {MasterComponent} from '../../../components/formation/master/master.component';
import {MasterRoutingModule} from './master-routing.module';



@NgModule({
  imports: [
    CommonModule,
    MasterRoutingModule
  ],
  exports: [
    MasterComponent
  ],
  declarations: [
    MasterComponent
  ],
  providers: [
  ]
})
export class MasterModule { }
