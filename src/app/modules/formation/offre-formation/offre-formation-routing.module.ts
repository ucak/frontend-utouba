import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { OffreFormationComponent } from 'src/app/components/formation/offre-formation/offre-formation.component';

const routes: Routes = [
  { path: '', component: OffreFormationComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class OffreFormationRoutingModule { }
