import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { OffreFormationRoutingModule } from './offre-formation-routing.module';
import { OffreFormationComponent } from 'src/app/components/formation/offre-formation/offre-formation.component';
import {TemplateModule} from '../../template/template.module';
import { TranslateModule } from '@ngx-translate/core';


@NgModule({
  exports: [
    OffreFormationComponent
  ],
  declarations: [
    OffreFormationComponent
  ],
  providers: [
  ],
  imports: [
    CommonModule,
    TemplateModule,
    TranslateModule,
    OffreFormationRoutingModule
  ]
})
export class OffreFormationModule { }
