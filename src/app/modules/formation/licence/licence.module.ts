import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {LicenceComponent} from '../../../components/formation/licence/licence.component';
import {LicenceRoutingModule} from './licence-routing.module';



@NgModule({
  imports: [
    CommonModule,
    LicenceRoutingModule
  ],
  exports: [
    LicenceComponent
  ],
  declarations: [
    LicenceComponent
  ],
  providers: [
  ]
})
export class LicenceModule { }
