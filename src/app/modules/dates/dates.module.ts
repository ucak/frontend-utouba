import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {DatesComponent} from '../../components/dates/dates.component';
import {DatesRoutingModule} from './dates-routing.module';



@NgModule({
  imports: [
    CommonModule,
    DatesRoutingModule
  ],
  exports: [
    DatesComponent
  ],
  declarations: [
    DatesComponent
  ],
  providers: [
  ]
})
export class DatesModule { }
