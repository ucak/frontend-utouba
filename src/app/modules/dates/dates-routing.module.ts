import {Component, NgModule} from '@angular/core';
import { CommonModule } from '@angular/common';
import {RouterModule, Routes} from '@angular/router';
import {BlogComponent} from '../../components/blog/blog.component';
import {DatesComponent} from '../../components/dates/dates.component';



const routes: Routes = [
  { path: '', component: DatesComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DatesRoutingModule { }
