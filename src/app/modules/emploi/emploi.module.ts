import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {EmploiRoutingModule} from './emploi-routing.module';
import {EmploiComponent} from '../../components/emploi/emploi.component';
import {TemplateModule} from '../template/template.module';



@NgModule({
    imports: [
        CommonModule,
        EmploiRoutingModule,
        TemplateModule
    ],
  exports: [
    EmploiComponent
  ],
  declarations: [
    EmploiComponent
  ],
  providers: [
  ]
})
export class EmploiModule { }
