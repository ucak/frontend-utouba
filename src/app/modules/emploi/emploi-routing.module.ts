import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {RouterModule, Routes} from '@angular/router';
import {DatesComponent} from '../../components/dates/dates.component';
import {EmploiComponent} from '../../components/emploi/emploi.component';



const routes: Routes = [
  { path: '', component: EmploiComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EmploiRoutingModule { }
