import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {TemplateComponent} from '../../shared/pages/template/template.component';
import {TranslateModule} from '@ngx-translate/core';



@NgModule({
  declarations: [TemplateComponent],
  imports: [
    CommonModule,
    TranslateModule
  ],
  exports: [TemplateComponent]
})
export class TemplateModule { }
