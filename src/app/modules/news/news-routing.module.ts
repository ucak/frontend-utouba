import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {RouterModule, Routes} from '@angular/router';
import {EvenementsComponent} from '../../components/evenements/evenements.component';
import {NewsComponent} from '../../components/news/news.component';



const routes: Routes = [
  { path: '', component: NewsComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class NewsRoutingModule { }
