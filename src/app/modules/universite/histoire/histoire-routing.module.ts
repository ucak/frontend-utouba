import { NgModule } from '@angular/core';
import { HistoireComponent } from 'src/app/components/universite/histoire/histoire.component';
import {RouterModule, Routes} from '@angular/router';

const routes: Routes = [
  { path: '', component: HistoireComponent },
];


@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HistoireRoutingModule { }
