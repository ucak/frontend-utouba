import { NgModule } from '@angular/core';
import { HistoireRoutingModule } from './histoire-routing.module';
import { HistoireComponent } from 'src/app/components/universite/histoire/histoire.component';
import {TemplateModule} from '../../template/template.module';
import {CommonModule} from '@angular/common';
import {TranslateModule} from '@ngx-translate/core';


@NgModule({
  exports: [
    HistoireComponent
  ],
  declarations: [
    HistoireComponent
  ],
  providers: [
  ],
  imports: [
    CommonModule,
    TemplateModule,
    HistoireRoutingModule,
    TranslateModule
  ]
})
export class HistoireModule { }
