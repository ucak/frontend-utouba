import { NgModule } from '@angular/core';
import { OrganigrammeRoutingModule } from './organigramme-routing.module';
import { OrganigrammeComponent } from 'src/app/components/universite/organigramme/organigramme.component';
import {CommonModule} from '@angular/common';


@NgModule({
  exports: [
    OrganigrammeComponent
  ],
  declarations: [
    OrganigrammeComponent
  ],
  providers: [
  ],
  imports: [
    CommonModule,
    OrganigrammeRoutingModule
  ]
})
export class OrganigrammeModule { }
