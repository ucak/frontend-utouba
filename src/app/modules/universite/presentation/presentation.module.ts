import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PresentationRoutingModule } from './presentation-routing.module';
import { PresentationComponent } from 'src/app/components/universite/presentation/presentation.component';
import {TemplateModule} from '../../template/template.module';
import {TranslateModule} from '@ngx-translate/core';
import {BidiModule} from '@angular/cdk/bidi';


@NgModule({
  exports: [
    PresentationComponent
  ],
  declarations: [
    PresentationComponent
  ],
  providers: [
  ],
    imports: [
        CommonModule,
        TemplateModule,
        PresentationRoutingModule,
        TranslateModule,
        BidiModule
    ]
})
export class PresentationModule { }
