import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {EvenementsRoutingModule} from './evenements-routing.module';
import {EvenementsComponent} from '../../components/evenements/evenements.component';



@NgModule({
  imports: [
    CommonModule,
    EvenementsRoutingModule
  ],
  exports: [
    EvenementsComponent
  ],
  declarations: [
    EvenementsComponent
  ],
  providers: [
  ]
})
export class EvenementsModule { }
