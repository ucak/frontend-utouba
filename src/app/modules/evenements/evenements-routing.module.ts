import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {RouterModule, Routes} from '@angular/router';
import {EmploiComponent} from '../../components/emploi/emploi.component';
import {EvenementsComponent} from '../../components/evenements/evenements.component';



const routes: Routes = [
  { path: '', component: EvenementsComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EvenementsRoutingModule { }
