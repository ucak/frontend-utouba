import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { OrganigrammeComponent } from 'src/app/components/universite/organigramme/organigramme.component';

const routes: Routes = [
  { path: '', component: OrganigrammeComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class OrganigrammeRoutingModule { }
