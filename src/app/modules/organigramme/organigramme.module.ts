import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { OrganigrammeRoutingModule } from './organigramme-routing.module';
import { OrganigrammeComponent } from 'src/app/components/universite/organigramme/organigramme.component';

@NgModule({
  imports: [
    CommonModule,
    OrganigrammeRoutingModule
  ],
  exports: [
    OrganigrammeComponent
  ],
  declarations: [
    OrganigrammeComponent
  ],
  providers: [
  ],
})
export class OrganigrammeModule { }