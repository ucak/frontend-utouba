import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FaculteRoutingModule } from './faculte-routing.module';
import { FaculteComponent } from 'src/app/components/faculte/faculte.component';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  imports: [
    CommonModule,
    FaculteRoutingModule,
    TranslateModule
  ],
  exports: [
    FaculteComponent
  ],
  declarations: [
    FaculteComponent
  ],
  providers: [
  ],
})
export class FaculteModule { }
