import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FaculteComponent } from 'src/app/components/faculte/faculte.component';

const routes: Routes = [
  { path: '', component: FaculteComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FaculteRoutingModule { }
