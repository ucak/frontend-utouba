import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SearchRoutingModule } from './search-routing.module';
import { SearchComponent } from 'src/app/components/search/search.component';
import { FormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    SearchComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    SearchRoutingModule
  ],
  exports: [
    SearchComponent
  ],
  providers: [
  ]
})
export class SearchModule { }
