import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { BibliothequeRoutingModule } from './bibliotheque-routing.module';
import { BibliothequeComponent } from 'src/app/components/bibliotheque/bibliotheque.component';
import {TemplateModule} from '../template/template.module';
import {TranslateModule} from '@ngx-translate/core';



@NgModule({
  imports: [
    CommonModule,
    TemplateModule,
    BibliothequeRoutingModule,
    TranslateModule
  ],
  exports: [
    BibliothequeComponent
  ],
  declarations: [
    BibliothequeComponent
  ],
  providers: [
  ]
})
export class BibliothequeModule { }
