/* Composant générique et réutilisable permettant d'avoir un modèle de page unique cf. PrésentationUniversité */
import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-template',
  templateUrl: './template.component.html',
  styleUrls: ['./template.component.scss']
})
export class TemplateComponent implements OnInit {

  @Input() title;
  @Input() subtitle;
  @Input() sideBarTitle;
  @Input() imageUrl;
  @Input() paragraph;
  constructor() { }

  ngOnInit(): void {}

}
