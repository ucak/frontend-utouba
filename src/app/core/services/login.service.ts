import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { delay } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  constructor() { }

  checkAccess(username: string, password: string): Observable<string> {

      return of(username + ' ' + password).pipe(delay(2000));
  }
}

