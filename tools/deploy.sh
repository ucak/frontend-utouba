#!/bin/bash
BASE_WEB_FOLDER=/home/ubuntu/ws
WEB_FOLDER_PATH=$BASE_WEB_FOLDER/$GITLAB_USER_LOGIN

if [ -z "$CI_OPEN_MERGE_REQUESTS" ]
then
  if [ -z "$PRODUCTION_ENV" ]
  then
    WEB_FOLDER_PATH=$BASE_WEB_FOLDER/develop
  else
    WEB_FOLDER_PATH=$BASE_WEB_FOLDER/html
  fi
else
  export CUSTOM_CI_MERGE_REQUEST_IID=$(echo "$CI_OPEN_MERGE_REQUESTS" | sed 's/[^0-9]*//g')
  CUSTOM_CI_API_URL="https://gitlab.com/api/v4/projects/${CI_PROJECT_ID}/merge_requests?iids[]=${CUSTOM_CI_MERGE_REQUEST_IID}"
  curl --header "PRIVATE-TOKEN: $CI_GITLAB_TOKEN" https://gitlab.com/api/v4/version
  curl -v --header "PRIVATE-TOKEN: $CI_GITLAB_TOKEN" -H "Accept: application/json" "${CUSTOM_CI_API_URL}" > output.txt
  TMP_CUSTOM_CI_USERNAME=$(cat output.txt | jq -r '.[] | .author | .username')
  CUSTOM_CI_USERNAME=$(echo "$TMP_CUSTOM_CI_USERNAME" | tr '[:upper:]' '[:lower:]')
  echo "GitLab Username: $CUSTOM_CI_USERNAME"
  export CUSTOM_CI_USERNAME
  if [ -z "$CUSTOM_CI_USERNAME" ]
  then
    echo "Cannot retrieve gitlab user"
    exit 1
  fi
  WEB_FOLDER_PATH=$BASE_WEB_FOLDER/$CUSTOM_CI_USERNAME
fi
env
#cleanup the web folder
ssh -o StrictHostKeyChecking=no -i $SSH_FILENAME $DEPLOY_USERNAME@$DEPLOY_HOSTNAME "rm -rf $WEB_FOLDER_PATH/*.*"
#deploy current version
scp -o StrictHostKeyChecking=no -r -i $SSH_FILENAME dist/frontend-utouba/* $DEPLOY_USERNAME@$DEPLOY_HOSTNAME:$WEB_FOLDER_PATH/
ssh -o StrictHostKeyChecking=no -i $SSH_FILENAME $DEPLOY_USERNAME@$DEPLOY_HOSTNAME "sudo -i chown -R ubuntu:ubuntu $WEB_FOLDER_PATH"
