# FrontendUtouba

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 10.2.0.

## Fiches et récupération projet pousser un feature
Le projet est hébérgé dans git, pour toute feature faut créer une branche à partir du `develop` pour traiter le feature.
Récupération du projet `git clone https://gitlab.com/ucak/frontend-utouba.git` 
`Tara` est utilisé pour la gestion des taches, afin de synchroniser les taches, y a quelques règles à respecter sur le nom des `fearures` dans `gitlab` et le `message de commit`.
Le nom d'une `feature` doit être comme suit `TASK-{numero-feature-dans-tara}` par exemple `TASK-1`.
Le message d'un `commit` doit être comme suit `TASK-{numero-feature-dans-tara}: libellé de la fiche `par exemple `TASK-1: En tant que PO je veux le plus beau site du monde`.

Après avoir récuperer le projet `git clone https://gitlab.com/ucak/frontend-utouba.git`, créer une feature branche en local via la commande
`git checkout -b TASK-{numero-feature-dans-tara}`
Committer les modifications `git commit -m "TASK-{numero-feature-dans-tara}: libellé de la fiche"` 
Pousser une nouvelle feature-branche `git push --set-upstream origin TASK-{numero-feature-dans-tara}`
Demander un merge request pour pousser les modifications dans `develop`. Ne pas oublier de supprimer le feature-branche dans gitlab une fois mergé.

## Configuration et démarrage projet
Exécuter le script `install.sh` qui exécute les tâches suivante:
    * Installe ou met à jour nvm 
    * Installe la version node de l'application qui est dans le fichier `.nvmrc`
    * Configure la bonne version de node
    * Installe `yarn` pour la gestion des dépendances et autres. 
Si tout ce passe bien alors vous être prêt à builder et démarrarer le projet, pour cela, il suffit de regarder le contenu du fichier `Makefile` qui contient différents commandes, par exemple taper dans votre console la commande `make run`  pour démarrer le projet en local. 


## Structure du projet 

Le projet est composé des dossiers suivants:
    * `components` qui contient les différents pages
    * `core`dans ce dossier on aura :
        * le dossier `guards` qui permettrons de définir les conditions pour accéder a une route. Par exemple bloquer les utilisateurs non connectés.
        * le dossier `services` qui contient les services backends appelés.
        * On l'enrichissera au fur et à mesure. Par exemple il peut contenir des `interceptors` plus tard!
    * `modules` contient les modules crées
    * `shared`contient tout ce qui peut être partagé :
        * `models` les objects backend et les constantes
        * `pages`  les pages qui peuvent être partagé entre différent composant pour éviter les copier coller.
        * `pipes` les differents pipes utilisés dans le composant.
A enrichir suivant l'évolution du projet.

    
## Creation d'une nouvelle route et page

Charger toutes les routes dans un même fichier peut causer quelques lenteurs lors du chargement du site, parce que la navigateur va télécharger un js qui est lourd et plus le projet est grand plus les statics générés seront volumineux. Pour faciliter ce chargment on utilise le `lazy-loading-ngmodules` ce qui va faire que lors du build angular va générer plusieurs fichiers qui seront moins lourds et le navigateur chargera les pages à la demande.

Dans le fichier `app-module.ts` on aura que les composants principaux qui se chargeront par defaut. Pour le moment on a `AppComponent, HomeComponent, NotFoundComponent, HeaderComponent, FooterComponent` on verra au fur et à mesure avec l'avancement du projet.
Pour créer Un nouveau module on utilise la commande  `ng g m modules/{module-name} --routing  --module=app` le module sera créer dans le dossier `modules` qui contiendra que les modules. Editer le fichier `app-module.ts, app-routing.module.ts` comme c'est fait avec le `module contact`.

Une fois le module fini, faut créer le composant qui va avec, pour cela créer le composant via la commande `ng g c components/{component-name}` qui sera crée dans le dossier `components`. Editer les fichiers mise à jour par la commande  comme c'est fait avec le `component contact`.


## Autres commandes

`ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Utilisation d'un proxy pour appeler backend

Pour appeler le backend depuis le dossier service, on est disons obliger de mettre l'url du serveur backend dans un js qui est effectivement visible depuis le navigateur. Pour eviter ce probleme  on a tendence a créer un proxy qui contient l'url final, la solution la plus courante consiste à mettre en place un nginx. De ce fait dans le fichier service lors de l'appel du backend, on fera juste par exemple `this.http.get('/api/v1/etudiant')` de ce l'appel sera à partir du host qui redirigera vers le nginx qui à son tour appele le backend.

Pour simuler ce comportement en mode dev, on a crée un fichier `proxy.conf.json` modifier le fichier `package.json` en ajoutant `--proxy-config proxy.conf.json` et la dependance en mode dev `http-proxy-agent` qui vont représenterons nos proxy en mode dev.


## Installation dépendance

Pour installer les dépendances, vous pouvez utiliser la commande make si non yarn
Via la commande make  `make install-dep  dep="nom-package"`. Un packege en mode dev `make install-dep-dev  dep="nom-package"` 
Exemple installation du package proxy en dev dependancy `make install-dep-dev  dep="http-proxy-agent"`
