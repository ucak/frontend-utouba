default: build

install:
	yarn

build: install
	yarn run build

lint:
	yarn run lint

build-prod: install
	yarn run prod

run:
	yarn run start

test: install
	yarn run test-headless

clean:
	rm -rf ./.tmp
	rm -Rf ./node_modules ./.tmp ./build
